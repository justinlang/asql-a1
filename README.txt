Advanced SQL Assignment 1
September 23, 2013
Justin Lang


INSTALLATION INSTRUCTIONS


1.  Execute database\init.sql to build the database, tables and procedures.
	a. The script creates a new user with permission to execute the new sprocs.
	b. All connections in application config files attempt to connect with the
		new user credentials.

		
2.  Build the ASQL.A1 solution in Visual Studio 2012.
	a. Run PlcServiceSimulator.exe with an IP address and an optional port as
		command line args.  Port 1000 is the default.
	b. Run OpcServerSimulator.exe with an IP address and an optional port as
		command line args.  Port 1000 is the default.
	c. Executables and dependencies are located in bin\Debug or bin\Release.
	d. Uses Nuget Package Manager to download dependencies during the initial build.
	

3. Build the ASQL.A1.WebClient solution in Visual Studio 2012
	a. The project is a .NET MVC4 web application.
	b. Either debug the website from within Visual Studio, or publish the application
		to a local IIS server.
			i. The published web app needs to run in a .NET 4 app pool.
			ii. The app needs permission to read/write to the inetpub directory.
			iii. There may be other kinks involved in the publishing process.  Debugging
				from the IDE may be easiest.
	c. Uses Nuget Package Manager to download dependencies during the initial build.

	
4. Build the ASQL.A1.Service solution in Visual Studio.
	a. Run a command prompt *as administrator*, and run: ASQL.A1.Service.exe --install
	b. Open the list of services in Task Manager, and start the new service called
		KwhEmailNotificationService
	c. Errors and logging info can be viewed in the Event Log
	d. Emails are sent on an hourly schedule.
	e. Uninstall the service by opening a command prompt *as administrator* and running:
		ASQL.A1.Service.exe --uninstall
	f. Executables and dependencies are located in bin\Debug or bin\Release.
	g. Uses Nuget Package Manager to download dependencies during the initial build.

	
KNOWN ISSUES:


1. The WebClient application either caches responses to AJAX GET requests internally,
	or else your browser may cache them.  I was not able to determine the root cause
	of this issue.  Thus, in Chrome, you often will need to do a hard
	refresh - CTRL-F5 - to get an updated report with recently added kWh usage data.
2. The Windows service does not start automatically after installation and needs
	to be started from the Windows Task Manager.