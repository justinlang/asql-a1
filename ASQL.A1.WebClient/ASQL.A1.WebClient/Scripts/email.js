﻿var Email = Email ? Email : (function () {

	var updateConfigEndpoint;

	var updateConfig = function (email, maxKwh) {

		$.ajax({
			type: 'PUT',
			data: { email: email, maxKwh: maxKwh },
			url: updateConfigEndpoint,
			cache: false
		}).done(function (data, textStatus, jqXHR) {

			if (data.hasError) {
				$('#emailConfigError').show();
				$('#emailConfigError').text('Please enter a valid ' + data.invalidField + '.');
			} else {
				$('#emailEntry').modal('hide');
			}

		}).fail(function (jqXHR, textStatus, errorThrown) {
			window.alert('An error occurred. Please try again.');
		});
	};

	return {
		setUpdateConfigEndpoint: function(endpoint) {
			updateConfigEndpoint = endpoint;
		},

		initializeSaveConfigButton: function () {

			$('#saveConfigButton').on('click', function () {
				var email = $('input#emailInput').val();
				var maxKwh = $('input#maxKwhInput').val();

				if (email == '' || maxKwh == '') {
					$('#emailConfigError').show();
					$('#emailConfigError').text('Please enter all required fields.');
				} else {
					$('#emailConfigError').hide();
					updateConfig(email, maxKwh);
				}
			});
		}
	};
})();