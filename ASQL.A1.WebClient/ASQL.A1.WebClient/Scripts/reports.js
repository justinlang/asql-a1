﻿var Reports = Reports ? Reports : (function () {

	// Request a specific report.
	// Hide the placeholder area and show a loading icon until we get the partial view back.
	var getReport = function (url, reportType, startDateTime, endDateTime) {
		$.ajax({
			type: 'GET',
			data: { reportType: reportType, startDateTime: startDateTime.toUTCString(), endDateTime: endDateTime.toUTCString() },
			url: url,
			cache: false
		}).done(function (data, textStatus, jqXHR) {

			if (data.hasError) {
				$('#reportError').show();
				$('#reportError').text(data.message);
				$('#loadingShim').hide();
			} else {
				$('#reportError').hide();

				$('#loadingShim').hide(300, function () {
					$('#reportPlaceholder').html(data);
					$('#reportPlaceholder').slideDown(150);
				});
			}
			
		}).fail(function (jqXHR, textStatus, errorThrown) {
			$('#loadingShim').hide();
			window.alert('An error occurred. Please try again.');
		});

		$('#reportPlaceholder').hide();
		$('#loadingShim').show();
	};

	// Construct a Date object using the datepicker and time text fields
	// given the container id.
	var constructDateFromDateTimeFields = function(id) {
		var date = $('#' + id + ' .datepicker').datepicker('getDate');

		var hours = $('#' + id + ' .hour').val();
		var minutes = $('#' + id + ' .minute').val();
		var seconds = $('#' + id + ' .second').val();

		date.setHours(hours);
		date.setMinutes(minutes);
		date.setSeconds(seconds);

		return date;
	};

	// Set the value of the datepicker field given the container id.
	var setDatePicker = function(id, date) {
		$('#' + id + ' .datepicker').datepicker("setDate", date);
	};

	// Set the value of the time fields given the container id.
	var setTimeFields = function (id, hours, minutes, seconds) {
		$('#' + id + ' .hour').val(hours);
		$('#' + id + ' .minute').val(minutes);
		$('#' + id + ' .second').val(seconds);
	};


	return {
		// Initialize chart for report views
		initializeChart: function(labelPoints, dataSet) {
			var context = $("#kwhChart").get(0).getContext("2d");	

			var data = {
				labels: labelPoints,
				datasets: [
					{
						fillColor: "#428bca",
						strokeColor: "#357ebd",
						pointColor: "#357ebd",
						pointStrokeColor: "#fff",
						data: dataSet
					}
				]
			};

			var chart = new Chart(context).Line(
				data,
				{
					scaleShowLabels: false,
					pointDotRadius: 1,
					animationSteps: 30
				}
			);
		},

		// Set the start and end datepicker fields to have default values.
		initializeDateTimeFields: function () {
			var today = new Date();
			var oneWeekAgo = new Date();
			oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
			
			setDatePicker('startDateTimeFields', oneWeekAgo);
			setDatePicker('endDateTimeFields', today);

			setTimeFields('startDateTimeFields', '00', '00', '00');
			setTimeFields('endDateTimeFields', today.getHours(), today.getMinutes(), today.getSeconds());
		},

		// Set up the onclick functions for the report buttons.
		// The clicked button becomes active.
		initializeReportButtons: function () {
			$('#reportButtons').on('click', 'button', function () {
				$('button[name="reportType"]').removeClass('active');
				$(this).addClass('active');
			});
		},

		// Fetch the report for the active button on page load.
		getDefaultReport: function (getReportUrl) {
			var reportType = $('button[name="reportType"].active').val();
			var startDateTime = constructDateFromDateTimeFields('startDateTimeFields');
			var endDateTime = constructDateFromDateTimeFields('endDateTimeFields');

			getReport(getReportUrl, reportType, startDateTime, endDateTime);
		},

		// Set up onclick for submit button.
		initializeSubmitButton: function (getReportUrl) {
			$('#submitButton').on('click', function () {
				var reportType = $('button[name="reportType"].active').val();
				var startDateTime = constructDateFromDateTimeFields('startDateTimeFields');
				var endDateTime = constructDateFromDateTimeFields('endDateTimeFields');

				getReport(getReportUrl, reportType, startDateTime, endDateTime);
			});
		}
	};
})();