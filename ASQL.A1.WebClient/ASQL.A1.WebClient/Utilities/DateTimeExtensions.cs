﻿/*
 * File:		DateTimeExtensions.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DateTimeExtensions static class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Utilities {

	/// <summary>
	/// Extension methods for DateTime objects.
	/// </summary>
	public static class DateTimeExtensions {

		/// <summary>
		/// Determines the DateTime for the start of the week for a DateTime, given also the desired day of the week
		/// to be the start of the week.
		/// 
		/// Reference: http://stackoverflow.com/questions/38039/how-can-i-get-the-datetime-for-the-start-of-the-week
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="startOfWeek"></param>
		/// <returns></returns>
		public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek) {

			int diff = dt.DayOfWeek - startOfWeek;
			if (diff < 0) {
				diff += 7;
			}

			return dt.AddDays(-1 * diff).Date;
		}
	}
}