﻿/*
 * File:		EmailValidator.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 24 2013
 * Description:	Contains EmailValidator class.
 */


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;


namespace ASQL.A1.WebClient.Utilities {

	/// <summary>
	/// Checks if strings are valid emails.
	/// 
	/// Reference: http://msdn.microsoft.com/en-us/library/01escwtf.aspx
	/// </summary>
	public class EmailValidator {

		private bool _invalid = false;

		public bool IsValidEmail(string strIn) {
			_invalid = false;
			if (String.IsNullOrEmpty(strIn))
				return false;

			// Use IdnMapping class to convert Unicode domain names. 
			try {
				strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
									  RegexOptions.None, TimeSpan.FromMilliseconds(200));
			} catch (RegexMatchTimeoutException) {
				return false;
			}

			if (_invalid)
				return false;

			// Return true if strIn is in valid e-mail format. 
			try {
				return Regex.IsMatch(strIn,
					  @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
					  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
					  RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
			} catch (RegexMatchTimeoutException) {
				return false;
			}
		}

		private string DomainMapper(Match match) {
			// IdnMapping class with default property values.
			IdnMapping idn = new IdnMapping();

			string domainName = match.Groups[2].Value;
			try {
				domainName = idn.GetAscii(domainName);
			} catch (ArgumentException) {
				_invalid = true;
			}
			return match.Groups[1].Value + domainName;
		}
	}
}