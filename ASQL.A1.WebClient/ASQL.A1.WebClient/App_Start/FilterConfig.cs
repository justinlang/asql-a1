﻿using System.Web;
using System.Web.Mvc;

namespace ASQL.A1.WebClient {
	public class FilterConfig {
		public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
			filters.Add(new HandleErrorAttribute());
		}
	}
}