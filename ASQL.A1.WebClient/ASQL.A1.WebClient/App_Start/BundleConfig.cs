﻿using System.Web.Optimization;

namespace ASQL.A1.WebClient {

	public class BundleConfig {

		public static void RegisterBundles(BundleCollection bundles) {

			bundles.Add(new ScriptBundle("~/Scripts/js").Include(
				"~/Scripts/bootstrap-datepicker.js",
				"~/Scripts/Chart.min.js",
				"~/Scripts/main.js",
				"~/Scripts/reports.js",
				"~/Scripts/email.js"
				));

			bundles.Add(new StyleBundle("~/Content/css").Include(
				"~/Content/bootstrap-datepicker.css",
				"~/Content/main.css"
				));
		}
	}
}