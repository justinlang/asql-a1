﻿/*
 * File:		ReportQuery.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ReportQuery static class.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Models {

	/// <summary>
	/// Represents a request to the server to generate a report.
	/// </summary>
	public class ReportQuery {

		[Required]
		public string ReportType { get; set; }

		[Required]
		public DateTime StartDateTime { get; set; }

		[Required]
		public DateTime EndDateTime { get; set; }
	}
}