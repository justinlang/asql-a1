﻿/*
 * File:		ReportsController.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ReportsController class.
 */


using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using ASQL.A1.WebClient.Domains.KwhUsage.Models;
using ASQL.A1.WebClient.Domains.KwhUsage.Services;
using ASQL.A1.WebClient.Models;
using ASQL.A1.WebClient.ViewModels;
using ASQL.A1.WebClient.Domains.EmailNotificationConfig.Services;


namespace ASQL.A1.WebClient.Controllers {

	/// <summary>
	/// Controller for report related requests.
	/// </summary>
	public class ReportsController : Controller {

		private readonly IKwhUsageService _kwhUsageService;
		private readonly IEmailNotificationConfigService _emailNotificationConfigService;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="kwhUsageService"></param>
		/// <param name="emailNotificationConfigService"></param>
		public ReportsController(
			IKwhUsageService kwhUsageService,
			IEmailNotificationConfigService emailNotificationConfigService
			) {

			_kwhUsageService = kwhUsageService;
			_emailNotificationConfigService = emailNotificationConfigService;
		}


		/// <summary>
		/// Return Report index view.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult Index() {
			return View();
		}


		/// <summary>
		/// Returns a partial view given a report query.
		/// </summary>
		/// <param name="reportQuery"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetReport(
			[System.Web.Http.FromUri]ReportQuery reportQuery
			) {

			// Probably this validation logic shouldn't be in the controller
			// It belongs to the report domain
			if (!this.ValidateDateRange(reportQuery.StartDateTime)) {
				return Json(
					new { hasError = true, message = LangResources.Lang.StartDateError },
					JsonRequestBehavior.AllowGet
					);
			}

			if (!this.ValidateDateRange(reportQuery.EndDateTime)) {
				return Json(
					new { hasError = true, message = LangResources.Lang.EndDateError },
					JsonRequestBehavior.AllowGet
					);
			}
			
			if (!this.ValidateDates(reportQuery.StartDateTime, reportQuery.EndDateTime)) {
				return Json(
					new { hasError = true, message = LangResources.Lang.StartAfterEndDateError },
					JsonRequestBehavior.AllowGet
					);
			}

			PartialViewResult reportPartialViewResult;
			if (this.TryGetReport(reportQuery, out reportPartialViewResult)) {
				return reportPartialViewResult;
			} else {
				return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
			}
		}


		/// <summary>
		/// Compare that start date is less than end date.
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		private bool ValidateDates(DateTime startDate, DateTime endDate) {
			return startDate < endDate;
		}

		/// <summary>
		/// Checks that a date falls in a certain range.
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		private bool ValidateDateRange(DateTime date) {
			return date > new DateTime(1900, 1, 1, 0, 0, 0) && date < new DateTime(2100, 12, 31, 0, 0, 0);
		}


		/// <summary>
		/// Updates the email config settings.
		/// </summary>
		/// <param name="email"></param>
		/// <param name="maxKwh"></param>
		/// <returns></returns>
		[HttpPut]
		public ActionResult UpdateConfig(
			string email,
			int maxKwh
			) {

				if (!_emailNotificationConfigService.IsValidEmail(email)) {
					return Json(new { hasError = true, invalidField = "email" });
				}

				if (!_emailNotificationConfigService.IsValidMaxKwh(maxKwh)) {
					return Json(new { hasError = true, invalidField = "maxKwh" });
				}

				if (_emailNotificationConfigService.TryUpdate(email, maxKwh)) {
					return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
				}

				return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
		}


		/// <summary>
		/// Attempts to parse the report query, fetch kWh usage data, and return the correct partial view.
		/// </summary>
		/// <param name="reportQuery"></param>
		/// <param name="reportPartialViewResult"></param>
		/// <returns></returns>
		private bool TryGetReport(ReportQuery reportQuery, out PartialViewResult reportPartialViewResult) {

			string[] validReportTypes = new string[] { "total", "weekly", "daily", "hourly" };

			if (Array.IndexOf(validReportTypes, reportQuery.ReportType) != -1) {

				int totalkwh = default(int);

				switch (reportQuery.ReportType) {
					case "total":
						totalkwh = _kwhUsageService.FetchTotalKwh(reportQuery.StartDateTime, reportQuery.EndDateTime);
						reportPartialViewResult = PartialView(
							"TotalKwhReport",
							new TotalKwhReport(totalkwh, reportQuery.StartDateTime, reportQuery.EndDateTime)
							);
						return true;

					case "weekly":
						reportPartialViewResult = GetKwhUsageIntervalReport(reportQuery, Interval.TimeInterval.Weekly);
						return true;

					case "daily":
						reportPartialViewResult = GetKwhUsageIntervalReport(reportQuery, Interval.TimeInterval.Daily);
						return true;

					case "hourly":
						reportPartialViewResult = GetKwhUsageIntervalReport(reportQuery, Interval.TimeInterval.Hourly);
						return true;

					default:
						break;
				}
			}

			reportPartialViewResult = null;
			return false;
		}


		/// <summary>
		/// Attempts to fetch kWh data and return an interval report.
		/// </summary>
		/// <param name="reportQuery"></param>
		/// <param name="timeInterval"></param>
		/// <returns></returns>
		private PartialViewResult GetKwhUsageIntervalReport(ReportQuery reportQuery, Interval.TimeInterval timeInterval) {

			IEnumerable<KwhTableRow> kwhTableRows = default(IEnumerable<KwhTableRow>);

			kwhTableRows = _kwhUsageService.FetchKwhForInterval(reportQuery.StartDateTime, reportQuery.EndDateTime, timeInterval)
				.Select(kwhUsageInterval => new KwhTableRow(kwhUsageInterval.ReadTime, kwhUsageInterval.Kwh));

			PartialViewResult reportPartialViewResult = PartialView(
				"KwhUsageIntervalReport",
				new KwhUsageIntervalReport(
					kwhTableRows,
					timeInterval,
					reportQuery.StartDateTime,
					reportQuery.EndDateTime
					)
				);

			return reportPartialViewResult;
		}
	}
}
