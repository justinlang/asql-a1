﻿/*
 * File:		SqlKwhUsageDataProvider.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains SqlKwhUsageDataProvider class.
 */


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.KwhUsage.Data;


namespace ASQL.A1.WebClient.Domains.KwhUsage.DataProviders {

	/// <summary>
	/// Uses a SqlConnection to fetch kWh usage data from a Sql database.
	/// </summary>
	internal class SqlKwhUsageDataProvider : IKwhUsageDataProvider {

		private readonly string _connectionString;


		/// <summary>
		/// Constructor.
		/// </summary>
		public SqlKwhUsageDataProvider() {
			
			_connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
		}


		/// <summary>
		/// Calls a sproc to fetch kWh usage data from a Sql database within a certain time range.
		/// </summary>
		/// <param name="lowerBound"></param>
		/// <param name="upperBound"></param>
		/// <returns></returns>
		IEnumerable<KwhUsageDto> IKwhUsageDataProvider.FetchByDateTimeRange(DateTime lowerBound, DateTime upperBound) {

			List<KwhUsageDto> dtos = new List<KwhUsageDto>();

			using (SqlConnection connection = new SqlConnection(_connectionString))
			using (SqlCommand command = new SqlCommand("FetchKwhUsageByDateTimeRange", connection)) {

				try {

					connection.Open();

					command.CommandType = System.Data.CommandType.StoredProcedure;

					command.Parameters.Add(new SqlParameter("@LowerBound", System.Data.SqlDbType.DateTime));
					command.Parameters["@LowerBound"].Value = lowerBound;

					command.Parameters.Add(new SqlParameter("@UpperBound", System.Data.SqlDbType.DateTime));
					command.Parameters["@UpperBound"].Value = upperBound;

					using (SqlDataReader reader = command.ExecuteReader()) {

						if (reader.HasRows) {

							int kwhUsageIdOrdinal = reader.GetOrdinal("KwhUsageId");
							int plcIdOrdinal = reader.GetOrdinal("PlcId");
							int readTimeOrdinal = reader.GetOrdinal("ReadTime");
							int kwhOrdinal = reader.GetOrdinal("Kwh");

							while (reader.Read()) {

								int kwhUsageId = reader.GetInt32(kwhUsageIdOrdinal);
								int plcId = reader.GetInt32(plcIdOrdinal);
								DateTime readTime = reader.GetDateTime(readTimeOrdinal);
								int kwh = reader.GetInt32(kwhOrdinal);

								dtos.Add(
									new KwhUsageDto(
										kwhUsageId,
										plcId,
										readTime,
										kwh
										)
									);
							}
						}
					}
				} catch (SqlException se) {

				}
			}

			return dtos;
		}
	}
}