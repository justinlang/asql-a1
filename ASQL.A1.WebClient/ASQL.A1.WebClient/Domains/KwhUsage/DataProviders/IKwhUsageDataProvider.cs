﻿/*
 * File:		IKwhUsageDataProvider.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IKwhUsageDataProvider interface.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.KwhUsage.Data;


namespace ASQL.A1.WebClient.Domains.KwhUsage.DataProviders {

	/// <summary>
	/// Contract for fetching kWh usage from persistent storage.
	/// </summary>
	internal interface IKwhUsageDataProvider {

		IEnumerable<KwhUsageDto> FetchByDateTimeRange(DateTime lowerBound, DateTime upperBound);
	}
}