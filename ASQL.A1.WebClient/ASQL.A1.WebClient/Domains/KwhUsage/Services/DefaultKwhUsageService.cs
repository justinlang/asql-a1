﻿/*
 * File:		DefaultKwhUsageService.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultKwhUsageService class.
 */


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ASQL.A1.WebClient.Domains.KwhUsage.Data;
using ASQL.A1.WebClient.Domains.KwhUsage.DataProviders;
using ASQL.A1.WebClient.Domains.KwhUsage.Models;
using ASQL.A1.WebClient.Utilities;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Services {

	/// <summary>
	/// A service for retrieving kWh usage information.
	/// </summary>
	internal class DefaultKwhUsageService : IKwhUsageService {

		private readonly IKwhUsageDataProvider _kwhUsageDataProvider;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="kwhUsageDataProvider"></param>
		public DefaultKwhUsageService(
			IKwhUsageDataProvider kwhUsageDataProvider
			) {
			_kwhUsageDataProvider = kwhUsageDataProvider;
		}


		/// <summary>
		/// Fetch total kWh usage for a time period.
		/// </summary>
		/// <param name="lowerBound"></param>
		/// <param name="upperBound"></param>
		/// <returns></returns>
		int IKwhUsageService.FetchTotalKwh(DateTime lowerBound, DateTime upperBound) {

			KwhUsageDto[] dtos = _kwhUsageDataProvider.FetchByDateTimeRange(lowerBound, upperBound).ToArray();

			if (dtos.Any()) {
				int totalKwh = dtos.Aggregate(0, (accumulator, dto) => accumulator += dto.Kwh);
				return totalKwh;
			}

			return 0;
		}


		/// <summary>
		/// Fetch kWh usage data in the form of a given time interval, for a specified time period.
		/// </summary>
		/// <param name="lowerBound"></param>
		/// <param name="upperBound"></param>
		/// <param name="timeInterval"></param>
		/// <returns></returns>
		IEnumerable<IKwhUsageInterval> IKwhUsageService.FetchKwhForInterval(DateTime lowerBound, DateTime upperBound, Interval.TimeInterval timeInterval) {

			KwhUsageDto[] dtos = _kwhUsageDataProvider.FetchByDateTimeRange(lowerBound, upperBound).ToArray();

			if (dtos.Any()) {

				IEnumerable<IKwhUsageInterval> kwhUsageIntervals = default(IEnumerable<IKwhUsageInterval>);

				switch (timeInterval) {
					case Interval.TimeInterval.Weekly:

						kwhUsageIntervals =
							dtos.GroupBy(
								dto => new { FirstDayOfWeek = dto.ReadTime.StartOfWeek(DayOfWeek.Sunday) },
								dto => dto.Kwh,
								(key, kwhs) =>
									new DefaultKwhUsageInterval(
										key.FirstDayOfWeek,
										kwhs.Aggregate(0, (accumulator, kwh) => accumulator += kwh)));

						kwhUsageIntervals.OrderBy(interval => interval.ReadTime);

						return kwhUsageIntervals;

					case Interval.TimeInterval.Daily:

						kwhUsageIntervals =
							dtos.GroupBy(
								dto => new { Year = dto.ReadTime.Year, Month = dto.ReadTime.Month, Day = dto.ReadTime.Day },
								dto => dto.Kwh,
								(key, kwhs) =>
									new DefaultKwhUsageInterval(
										new DateTime(key.Year, key.Month, key.Day),
										kwhs.Aggregate(0, (accumulator, kwh) => accumulator += kwh)));

						kwhUsageIntervals.OrderBy(interval => interval.ReadTime);

						return kwhUsageIntervals;

					case Interval.TimeInterval.Hourly:

						kwhUsageIntervals =
							dtos.GroupBy(
								dto => new { Year = dto.ReadTime.Year, Month = dto.ReadTime.Month, Day = dto.ReadTime.Day, Hour = dto.ReadTime.Hour },
								dto => dto.Kwh,
								(key, kwhs) =>
									new DefaultKwhUsageInterval(
										new DateTime(key.Year, key.Month, key.Day, key.Hour, 0, 0),
										kwhs.Aggregate(0, (accumulator, kwh) => accumulator += kwh)));

						kwhUsageIntervals.OrderBy(interval => interval.ReadTime.Date)
							.ThenBy(interval => interval.ReadTime.TimeOfDay);

						return kwhUsageIntervals;
				}
			}

			return new List<IKwhUsageInterval>();
		}
	}
}