﻿/*
 * File:		IKwhUsageService.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IKwhUsageService interface.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.KwhUsage.Models;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Services {

	/// <summary>
	/// Contract for a kWh usage service.
	/// </summary>
	public interface IKwhUsageService {

		int FetchTotalKwh(DateTime lowerBound, DateTime upperBound);
		IEnumerable<IKwhUsageInterval> FetchKwhForInterval(DateTime lowerBound, DateTime upperBound, Interval.TimeInterval timeInterval);
	}
}