﻿/*
 * File:		IKwhUsageInterval.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IKwhUsageInterval interface.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Models {

	/// <summary>
	/// Contract for describing kWh usage.
	/// </summary>
	public interface IKwhUsageInterval {

		DateTime ReadTime { get; }
		int Kwh { get; }
	}
}