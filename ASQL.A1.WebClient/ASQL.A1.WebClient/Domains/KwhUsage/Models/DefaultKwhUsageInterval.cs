﻿/*
 * File:		DefaultKwhUsageInterval.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultKwhUsageInterval class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.KwhUsage.Data;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Models {

	/// <summary>
	/// Default implementation of IKwhUsageInterval.
	/// </summary>
	internal class DefaultKwhUsageInterval : IKwhUsageInterval {

		private readonly DateTime _readTime;
		private readonly int _kwh;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="readTime"></param>
		/// <param name="kwh"></param>
		public DefaultKwhUsageInterval(
			DateTime readTime,
			int kwh
			) {

			_readTime = readTime;
			_kwh = kwh;
		}

		DateTime IKwhUsageInterval.ReadTime {
			get { return _readTime; }
		}

		int IKwhUsageInterval.Kwh {
			get { return _kwh; }
		}
	}
}