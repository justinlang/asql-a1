﻿/*
 * File:		Interval.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains Interval class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Models {

	/// <summary>
	/// Contains an enum describing how to represent kWh usage as a time interval.
	/// </summary>
	public class Interval {

		public enum TimeInterval {
			Weekly = 0,
			Daily,
			Hourly
		}
	}
}