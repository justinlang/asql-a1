﻿/*
 * File:		KwhUsageDto.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains KwhUsageDto class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.KwhUsage.Data {

	/// <summary>
	/// Represents kWh usage data retrieved from the database.
	/// </summary>
	internal class KwhUsageDto {

		private readonly int _kwhUsageId;
		private readonly int _plcId;
		private readonly DateTime _readTime;
		private readonly int _kwh;

		/// <summary>
		/// Constructor.
		/// </summary>
		public KwhUsageDto() { }


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="kwhUsageId"></param>
		/// <param name="plcId"></param>
		/// <param name="readTime"></param>
		/// <param name="kwh"></param>
		public KwhUsageDto(
			int kwhUsageId,
			int plcId,
			DateTime readTime,
			int kwh
			) {

			_kwhUsageId = kwhUsageId;
			_plcId = plcId;
			_readTime = readTime;
			_kwh = kwh;
		}

		public int KwhUsageId {
			get { return _kwhUsageId; }
		}

		public int PlcId {
			get { return _plcId; }
		}

		public DateTime ReadTime {
			get { return _readTime; }
		}

		public int Kwh {
			get { return _kwh; }
		}
	}
}