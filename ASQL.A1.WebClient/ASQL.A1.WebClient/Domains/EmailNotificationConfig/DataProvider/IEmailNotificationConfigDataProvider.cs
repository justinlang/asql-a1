﻿/*
 * File:		IEmailNotificationConfigDataProvider.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IEmailNotificationConfigDataProvider interface.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.EmailNotificationConfig.Data;



namespace ASQL.A1.WebClient.Domains.EmailNotificationConfig.DataProvider {

	/// <summary>
	/// Contract for updating email config data from persistent storage.
	/// </summary>
	public interface IEmailNotificationConfigDataProvider {

		bool TryUpdate(EmailNotificationConfigDto dto);
	}
}