﻿/*
 * File:		SqlEmailNotificationConfigDataProvider.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains SqlEmailNotificationConfigDataProvider class.
 */


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.EmailNotificationConfig.DataProvider {

	/// <summary>
	/// Uses a SqlConnection to access email config data from a Sql database.
	/// </summary>
	public class SqlEmailNotificationConfigDataProvider : IEmailNotificationConfigDataProvider {

		private readonly string _connectionString;


		/// <summary>
		/// Constructor.
		/// </summary>
		public SqlEmailNotificationConfigDataProvider() {

			_connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
		}


		/// <summary>
		/// Calls a sproc to try updating email config data in a Sql database.
		/// </summary>
		/// <param name="dto"></param>
		/// <returns></returns>
		bool IEmailNotificationConfigDataProvider.TryUpdate(Data.EmailNotificationConfigDto dto) {

			using (SqlConnection connection = new SqlConnection(_connectionString))
			using (SqlCommand command = new SqlCommand("UpdateEmailNotificationConfig", connection)) {

				try {
					connection.Open();

					command.CommandType = System.Data.CommandType.StoredProcedure;

					command.Parameters.Add(new SqlParameter("@Email", System.Data.SqlDbType.VarChar));
					command.Parameters["@Email"].Value = dto.Email;

					command.Parameters.Add(new SqlParameter("@MaxKwh", System.Data.SqlDbType.Int));
					command.Parameters["@MaxKwh"].Value = dto.MaxKwh;

					int rowsAffected = command.ExecuteNonQuery();
					return rowsAffected > 0;

				} catch (SqlException se) {
					
				}
			}

			return false;
		}
	}
}