﻿/*
 * File:		EmailNotificationConfigDto.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains EmailNotificationConfigDto class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.EmailNotificationConfig.Data {


	/// <summary>
	/// Represents email configuration data retrieved from the database.
	/// </summary>
	public class EmailNotificationConfigDto {

		private readonly string _email;
		private readonly int _maxKwh;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="email"></param>
		/// <param name="maxKwh"></param>
		public EmailNotificationConfigDto(
			string email,
			int maxKwh
			) {

			_email = email;
			_maxKwh = maxKwh;
		}

		public int MaxKwh {
			get { return _maxKwh; }
		}

		public string Email {
			get { return _email; }
		}
	}
}