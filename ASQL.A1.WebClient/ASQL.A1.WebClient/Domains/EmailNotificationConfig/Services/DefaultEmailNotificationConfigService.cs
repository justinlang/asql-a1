﻿/*
 * File:		DefaultEmailNotificationConfigService.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultEmailNotificationConfigService class.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.EmailNotificationConfig.DataProvider;
using ASQL.A1.WebClient.Utilities;



namespace ASQL.A1.WebClient.Domains.EmailNotificationConfig.Services {

	/// <summary>
	/// Default implementation of a service which attempts to update email config data.
	/// </summary>
	public class DefaultEmailNotificationConfigService : IEmailNotificationConfigService {

		private readonly IEmailNotificationConfigDataProvider _dataProvider;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="dataProvider"></param>
		public DefaultEmailNotificationConfigService(
			IEmailNotificationConfigDataProvider dataProvider
			) {

			_dataProvider = dataProvider;
		}
		

		/// <summary>
		/// If the email and kWh value are valid, try updating the values in persistent storage.
		/// </summary>
		/// <param name="email"></param>
		/// <param name="maxKwh"></param>
		/// <returns></returns>
		bool IEmailNotificationConfigService.TryUpdate(string email, int maxKwh) {

			IEmailNotificationConfigService @this = this;

			if (@this.IsValidEmail(email) && @this.IsValidMaxKwh(maxKwh)) {
				return _dataProvider.TryUpdate(new Data.EmailNotificationConfigDto(email, maxKwh));
			}

			return false;
		}


		/// <summary>
		/// Checks if the kWh value is a valid non-negative integer.
		/// </summary>
		/// <param name="maxKwh"></param>
		/// <returns></returns>
		bool IEmailNotificationConfigService.IsValidMaxKwh(int maxKwh) {
			return maxKwh >= 0;
		}


		/// <summary>
		/// Checks if the string is a valid email address.
		/// </summary>
		/// <param name="email"></param>
		/// <returns></returns>
		bool IEmailNotificationConfigService.IsValidEmail(string email) {
			return new EmailValidator().IsValidEmail(email);
		}
	}
}