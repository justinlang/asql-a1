﻿/*
 * File:		IEmailNotificationConfigService.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IEmailNotificationConfigService interface.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.Domains.EmailNotificationConfig.Services {

	/// <summary>
	/// Contract for service which updates email config data.
	/// </summary>
	public interface IEmailNotificationConfigService {

		bool TryUpdate(string email, int maxKwh);
		bool IsValidMaxKwh(int maxKwh);
		bool IsValidEmail(string email);
	}
}