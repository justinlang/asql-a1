﻿/*
 * File:		StructureMapDependencyResolver.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains StructureMapDependencyResolver class.
 */


using System;
using System.Collections.Generic;
using System.Web.Mvc;
using StructureMap;


namespace ASQL.A1.WebClient.IoC {

	/// <summary>
	/// Allows StructureMap to handle dependency resolution in .NET MVC framework.
	/// </summary>
	public class StructureMapDependencyResolver : IDependencyResolver {

		private readonly IContainer _container;

		public StructureMapDependencyResolver(IContainer container) {
			_container = container;
		}

		object IDependencyResolver.GetService(Type serviceType) {
			if (serviceType.IsAbstract || serviceType.IsInterface) {
				return _container.TryGetInstance(serviceType);
			} else {
				return _container.GetInstance(serviceType);
			}
		}

		IEnumerable<object> IDependencyResolver.GetServices(Type serviceType) {
			foreach (object instance in _container.GetAllInstances(serviceType)) {
				yield return instance;
			}
		}
	}
}