﻿/*
 * File:		IoCConfig.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IoCConfig class.
 */


using System.Web.Mvc;
using ASQL.A1.WebClient.Domains.EmailNotificationConfig.DataProvider;
using ASQL.A1.WebClient.Domains.EmailNotificationConfig.Services;
using ASQL.A1.WebClient.Domains.KwhUsage.DataProviders;
using ASQL.A1.WebClient.Domains.KwhUsage.Services;
using StructureMap;


namespace ASQL.A1.WebClient.IoC {

	/// <summary>
	/// Represents inversion of control configuration settings.
	/// </summary>
	public class IoCConfig {

		/// <summary>
		/// Initialize StructureMap IoC container.
		/// </summary>
		/// <returns></returns>
		public static IContainer Initialize() {

			IContainer container = new Container(config => {
				config.Scan(
					scan => {
						scan.TheCallingAssembly();
						scan.WithDefaultConventions();
					});
				
				// Enable StructureMap to instantiate controllers
				config.For<IControllerActivator>().Singleton().Use<StructureMapControllerActivator>();

				// Singletons
				config.For<IKwhUsageDataProvider>().Singleton().Use<SqlKwhUsageDataProvider>();
				config.For<IEmailNotificationConfigDataProvider>().Singleton().Use<SqlEmailNotificationConfigDataProvider>();

				// Fresh instances
				config.For<IKwhUsageService>().Use<DefaultKwhUsageService>();
				config.For<IEmailNotificationConfigService>().Use<DefaultEmailNotificationConfigService>();
			});

			return container;
		}
	}
}