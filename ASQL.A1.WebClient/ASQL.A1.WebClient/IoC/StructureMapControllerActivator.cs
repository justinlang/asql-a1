﻿/*
 * File:		StructureMapControllerActivator.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains StructureMapControllerActivator class.
 */


using System;
using System.Web.Mvc;
using StructureMap;


namespace ASQL.A1.WebClient.IoC {

	/// <summary>
	/// Allows StructureMap to handle instantiation of controllers in .NET MVC framework.
	/// </summary>
	public class StructureMapControllerActivator : IControllerActivator {

		private readonly IContainer _container;

		public StructureMapControllerActivator(IContainer container) {
			_container = container;
		}

		IController IControllerActivator.Create(System.Web.Routing.RequestContext requestContext, Type controllerType) {
			return _container.GetInstance(controllerType) as IController;
		}
	}
}