﻿/*
 * File:		KwhTableRow.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains KwhTableRow class.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.ViewModels {

	/// <summary>
	/// Represents a row in a partial view report for kWh usage.
	/// </summary>
	public class KwhTableRow {

		private readonly DateTime _readDateTime;
		private readonly int _kwh;

		public KwhTableRow(
			DateTime readDateTime,
			int kwh
			) {

			_readDateTime = readDateTime;
			_kwh = kwh;
		}

		[DisplayFormat(DataFormatString = "{0: yyyy/MM/dd}")]
		public DateTime ReadDate {
			get { return _readDateTime; }
		}

		[DisplayFormat(DataFormatString = "{0: HH:mm}")]
		public DateTime ReadTime {
			get { return _readDateTime; }
		}

		public int Kwh {
			get { return _kwh; }
		}
	}
}