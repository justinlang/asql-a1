﻿/*
 * File:		TotalKwhReport.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains TotalKwhReport class.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ASQL.A1.WebClient.ViewModels {

	/// <summary>
	/// Represents the data included in a total kWh report partial view.
	/// </summary>
	public class TotalKwhReport {

		private readonly int _totalKwh;
		private readonly DateTime _startDateTime;
		private readonly DateTime _endDateTime;

		public TotalKwhReport(
			int totalKwh,
			DateTime startDateTime,
			DateTime endDateTime
			) {

			_totalKwh = totalKwh;
			_startDateTime = startDateTime;
			_endDateTime = endDateTime;
		}

		public int TotalKwh {
			get { return _totalKwh; }
		}

		[DisplayFormat(DataFormatString = "{0: yyyy/MM/dd HH:mm:ss}")]
		public DateTime StartDateTime {
			get { return _startDateTime; }
		}

		[DisplayFormat(DataFormatString = "{0: yyyy/MM/dd HH:mm:ss}")]
		public DateTime EndDateTime {
			get { return _endDateTime; }
		}
	}
}