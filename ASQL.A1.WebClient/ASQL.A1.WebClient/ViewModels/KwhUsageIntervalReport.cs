﻿/*
 * File:		KwhUsageIntervalReport.cs
 * Project:		ASQL.A1.WebClient
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains KwhUsageIntervalReport class.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ASQL.A1.WebClient.Domains.KwhUsage.Models;


namespace ASQL.A1.WebClient.ViewModels {

	/// <summary>
	/// Represents the data included in a kWh interval report partial view.
	/// </summary>
	public class KwhUsageIntervalReport {

		private readonly IEnumerable<KwhTableRow> _kwhTableRows;
		private readonly Interval.TimeInterval _timeInterval;
		private readonly DateTime _startDateTime;
		private readonly DateTime _endDateTime;

		public KwhUsageIntervalReport(
			IEnumerable<KwhTableRow> kwhTableRows,
			Interval.TimeInterval timeInterval,
			DateTime startDateTime,
			DateTime endDateTime
			) {

			_kwhTableRows = kwhTableRows;
			_timeInterval = timeInterval;
			_startDateTime = startDateTime;
			_endDateTime = endDateTime;
		}

		public IEnumerable<KwhTableRow> KwhTableRows {
			get { return _kwhTableRows; }
		}

		[DisplayFormat(DataFormatString = "{0: yyyy/MM/dd HH:mm:ss}")]
		public DateTime StartDateTime {
			get { return _startDateTime; }
		}

		[DisplayFormat(DataFormatString = "{0: yyyy/MM/dd HH:mm:ss}")]
		public DateTime EndDateTime {
			get { return _endDateTime; }
		}

		public string ReportHeader {
			get {
				switch (_timeInterval) {
					case Interval.TimeInterval.Daily:
						return LangResources.Lang.DailyKwhReport;
					case Interval.TimeInterval.Hourly:
						return LangResources.Lang.HourlyKwhConsumption;
					case Interval.TimeInterval.Weekly:
						return LangResources.Lang.WeeklyKwhReport;
					default:
						return string.Empty;
				}
			}
		}

		public Interval.TimeInterval TimeInterval {
			get { return _timeInterval; }
		}
	}
}