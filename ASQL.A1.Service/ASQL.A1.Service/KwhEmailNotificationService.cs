﻿/*
 * File:		KwhEmailNotificationService.cs
 * Project:		ASQL.A1.Service
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains KwhEmailNotificationService class.
 */


using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using Quartz;
using Quartz.Impl;


namespace ASQL.A1.Service {

	/// <summary>
	/// Inherits from ServiceBase and overrides methods which allow the application to run as a Windows service.
	/// </summary>
	partial class KwhEmailNotificationService : ServiceBase {

		private readonly IScheduler _scheduler;


		/// <summary>
		/// Constructor.
		/// </summary>
		public KwhEmailNotificationService() {

			InitializeComponent();
			_scheduler = new StdSchedulerFactory().GetScheduler();
		}


		/// <summary>
		/// Sets up a recurring Quartz.NET job and schedules the job for execution.
		/// </summary>
		/// <param name="args"></param>
		protected override void OnStart(string[] args) {
			
			IJobDetail jobDetail = JobBuilder.Create(typeof(EmailNotificationJob)).Build();

			string cronString = ConfigurationManager.AppSettings["CronPollingString"];
			ITrigger trigger = TriggerBuilder.Create().WithCronSchedule(cronString).Build();

			_scheduler.ScheduleJob(jobDetail, trigger);
			_scheduler.Start();

			EventLog.WriteEntry("Starting service...");
		}


		/// <summary>
		/// Stops the service.
		/// </summary>
		protected override void OnStop() {

			_scheduler.Shutdown();

			EventLog.WriteEntry("Shutting down...");
		}
	}
}
