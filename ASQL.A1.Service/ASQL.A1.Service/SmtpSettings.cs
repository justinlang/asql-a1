﻿/*
 * File:		SmtpSettings.cs
 * Project:		ASQL.A1.Service
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains SmtpSettings class.
 */


namespace ASQL.A1.Service {

	/// <summary>
	/// Represents the Smtp settings stored in application configuration.
	/// </summary>
	public class SmtpSettings {

		private readonly string _fromAddress;
		private readonly string _server;
		private readonly int _port;
		private readonly string _username;
		private readonly string _password;
		private readonly bool _enableSsl;

		public string FromAddress {
			get { return _fromAddress; }
		}

		public string Server {
			get { return _server; }
		}

		public int Port {
			get { return _port; }
		}

		public string Username {
			get { return _username; }
		}

		public string Password {
			get { return _password; }
		}

		public bool EnableSsl {
			get { return _enableSsl; }
		}


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="fromAddress"></param>
		/// <param name="server"></param>
		/// <param name="port"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="enableSsl"></param>
		public SmtpSettings(
			string fromAddress,
			string server,
			int port,
			string username,
			string password,
			bool enableSsl
			) {

			_fromAddress = fromAddress;
			_server = server;
			_port = port;
			_username = username;
			_password = password;
			_enableSsl = enableSsl;
		}
	}
}