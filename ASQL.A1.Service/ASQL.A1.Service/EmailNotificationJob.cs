﻿/*
 * File:		EmailNotificationJob.cs
 * Project:		ASQL.A1.Service
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains EmailNotificationJob class.
 */


using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using Quartz;


namespace ASQL.A1.Service {

	/// <summary>
	/// Implements the Quartz.NET IJob interface.
	/// 
	/// Represents a recurring scheduled process which fetches kWh usage data, and fetches email config data,
	/// and sends an email according to Smtp setting in the configuration file, if the usage has exceeded the
	/// max kWh value in the last hour.
	/// 
	/// TODO This class needs major refactoring.  Should at least separate out the database logic into
	/// a separate class, or better yet share it between the WebClient and the Service and the OpcServerSimulator.
	/// </summary>
	public class EmailNotificationJob : IJob {

		private readonly EventLog _eventLog;
		private readonly string _connectionString;
		private readonly SmtpSettings _smtpSettings;
		private const int DEFAULT_SMTP_PORT = 587;
		private const bool DEFAULT_ENABLE_SSL = true;


		/// <summary>
		/// Constructor.
		/// 
		/// Fetches application configuration settings and stores them in class-level fields.
		/// </summary>
		public EmailNotificationJob() {
			_connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

			int smtpPort = DEFAULT_SMTP_PORT;
			bool enableSsl = DEFAULT_ENABLE_SSL;

			_smtpSettings = new SmtpSettings(
				ConfigurationManager.AppSettings["SmtpFromAddress"],
				ConfigurationManager.AppSettings["SmtpServer"],
				Int32.TryParse(ConfigurationManager.AppSettings["SmtpPort"], out smtpPort) ? smtpPort : DEFAULT_SMTP_PORT,
				ConfigurationManager.AppSettings["SmtpUsername"],
				ConfigurationManager.AppSettings["SmtpPassword"],
				Boolean.TryParse(ConfigurationManager.AppSettings["SmtpEnableSsl"], out enableSsl) ? enableSsl : DEFAULT_ENABLE_SSL
				);

			_eventLog = new EventLog();
			_eventLog.Source = "KwhEmailNotificationService_Job";
		}


		/// <summary>
		/// Fetches email config data and kWh usage for the last hour.
		/// If the kWh usage exceeds the max kWh, an email is sent to the user.
		/// </summary>
		/// <param name="context"></param>
		void IJob.Execute(IJobExecutionContext context) {

			try {

				EmailNotificationConfigDto emailConfigDto = FetchEmailConfig();

				if (!this.ValidateEmail(emailConfigDto.Email)) {
					LogInfo("No email address could be retrieved.");
					return;
				}

				var lastHour =
					new DateTime(
						DateTime.Now.Year,
						DateTime.Now.Month,
						// Handles midnight cases
						DateTime.Now.Hour == 0 ? DateTime.Now.Day - 1 : DateTime.Now.Day,
						DateTime.Now.Hour == 0 ? 23 : DateTime.Now.Hour - 1,
						0,
						0
					);

				var currentHour = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);

				KwhUsageDto[] kwhUsageDtos = FetchByDateTimeRange(lastHour, currentHour).ToArray();

				if (kwhUsageDtos.Any()) {
					int lastHourKwh = kwhUsageDtos.Aggregate(0, (accumulator, dto) => accumulator += dto.Kwh);

					if (lastHourKwh > emailConfigDto.MaxKwh) {
						SendMail(lastHourKwh, emailConfigDto.MaxKwh, lastHour, currentHour, emailConfigDto.Email);
					}
				}

			} catch (Exception ex) {
				throw new JobExecutionException(ex);
			}
		}


		private bool ValidateEmail(string email) {
			return !string.IsNullOrEmpty(email);
		}


		/// <summary>
		/// Sends an email according to the email config data and Smtp configuration settings.
		/// </summary>
		/// <param name="lastHourKwh"></param>
		/// <param name="maxKwh"></param>
		/// <param name="lastHour"></param>
		/// <param name="currentHour"></param>
		/// <param name="email"></param>
		private void SendMail(int lastHourKwh, int maxKwh, DateTime lastHour, DateTime currentHour, string email) {

			try {

				SmtpClient smtpClient = new SmtpClient(_smtpSettings.Server, _smtpSettings.Port) {
					Credentials = new NetworkCredential(_smtpSettings.Username, _smtpSettings.Password),
					EnableSsl = _smtpSettings.EnableSsl
				};

				smtpClient.Send(
					_smtpSettings.FromAddress,
					email,
					"kWh Hourly Consumption Exceeded",
					string.Format("{0} - {1}{2}Kwh exceeded the maximum in the last hour.{3}kWh consumption: {4}{5}Max kWh: {6}",
						lastHour,
						currentHour,
						Environment.NewLine,
						Environment.NewLine,
						lastHourKwh,
						Environment.NewLine,
						maxKwh
						)
					);

				LogInfo("Email sent successfully.");

			} catch (SmtpFailedRecipientException failedRecepientsEx) {
				LogError(failedRecepientsEx.Message, failedRecepientsEx.StackTrace);

			} catch (SmtpException smtpEx) {
				LogError(smtpEx.Message, smtpEx.StackTrace);

			} catch (Exception ex) {
				LogError(ex.Message, ex.StackTrace);
				throw ex;
			}
		}


		/// <summary>
		/// Logs information using the EventLog.
		/// </summary>
		/// <param name="message"></param>
		private void LogInfo(string message) {
			_eventLog.WriteEntry(message, EventLogEntryType.Information);
		}


		/// <summary>
		/// Logs an error using the EventLog.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="stackTrace"></param>
		private void LogError(string message, string stackTrace) {
			_eventLog.WriteEntry(message + Environment.NewLine + stackTrace, EventLogEntryType.Error);
		}


		/// <summary>
		/// Fetch email config data from a Sql database using a SqlConnection and a sproc.
		/// </summary>
		/// <returns></returns>
		private EmailNotificationConfigDto FetchEmailConfig() {

			EmailNotificationConfigDto emailConfigDto;

			using (SqlConnection connection = new SqlConnection(_connectionString))
			using (SqlCommand command = new SqlCommand("FetchEmailNotificationConfig", connection)) {

				try {

					connection.Open();
					command.CommandType = System.Data.CommandType.StoredProcedure;

					using (SqlDataReader reader = command.ExecuteReader()) {

						if (reader.HasRows) {

							int emailOrdinal = reader.GetOrdinal("Email");
							int maxKwhOrdinal = reader.GetOrdinal("MaxKwh");

							while (reader.Read()) {

								string email = reader.GetString(emailOrdinal);
								int maxKwh = reader.GetInt32(maxKwhOrdinal);

								LogInfo("Fetched email config successfully.");

								emailConfigDto = new EmailNotificationConfigDto(email, maxKwh);
								return emailConfigDto;
							}
						}
					}
				} catch (SqlException se) {
					LogError(se.Message, se.StackTrace);
					throw se;
				}
			}

			return default(EmailNotificationConfigDto); ;
		}


		/// <summary>
		/// Fetch kWh usage data from a Sql database using a SqlConnection and a sproc,
		/// for a given time period.
		/// </summary>
		/// <param name="lowerBound"></param>
		/// <param name="upperBound"></param>
		/// <returns></returns>
		IEnumerable<KwhUsageDto> FetchByDateTimeRange(DateTime lowerBound, DateTime upperBound) {

			List<KwhUsageDto> dtos = new List<KwhUsageDto>();

			using (SqlConnection connection = new SqlConnection(_connectionString))
			using (SqlCommand command = new SqlCommand("FetchKwhUsageByDateTimeRange", connection)) {

				try {

					connection.Open();

					command.CommandType = System.Data.CommandType.StoredProcedure;

					command.Parameters.Add(new SqlParameter("@LowerBound", System.Data.SqlDbType.DateTime));
					command.Parameters["@LowerBound"].Value = lowerBound;

					command.Parameters.Add(new SqlParameter("@UpperBound", System.Data.SqlDbType.DateTime));
					command.Parameters["@UpperBound"].Value = upperBound;

					using (SqlDataReader reader = command.ExecuteReader()) {

						if (reader.HasRows) {

							int kwhUsageIdOrdinal = reader.GetOrdinal("KwhUsageId");
							int plcIdOrdinal = reader.GetOrdinal("PlcId");
							int readTimeOrdinal = reader.GetOrdinal("ReadTime");
							int kwhOrdinal = reader.GetOrdinal("Kwh");

							while (reader.Read()) {

								int kwhUsageId = reader.GetInt32(kwhUsageIdOrdinal);
								int plcId = reader.GetInt32(plcIdOrdinal);
								DateTime readTime = reader.GetDateTime(readTimeOrdinal);
								int kwh = reader.GetInt32(kwhOrdinal);

								dtos.Add(
									new KwhUsageDto(
										kwhUsageId,
										plcId,
										readTime,
										kwh
										)
									);
							}
						}
					}
				} catch (SqlException se) {
					LogError(se.Message, se.StackTrace);
					throw se;
				}
			}

			return dtos;
		}
	}
}
