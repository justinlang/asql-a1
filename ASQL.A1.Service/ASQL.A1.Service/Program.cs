﻿/*
 * File:		Program.cs
 * Project:		ASQL.A1.Service
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains Program class.
 */


using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;


namespace ASQL.A1.Service {

	/// <summary>
	/// Represents the application.
	/// </summary>
	static class Program {

		/// <summary>
		/// The main entry point for the application.
		/// 
		/// If the application is run from the command line, and the parameters --install or --uninstall are given,
		/// The app is either installed or uninstalled as a Windows service.
		/// 
		/// Otherwise, if the service has been installed, the application executes as a service.
		/// 
		/// Reference: http://stackoverflow.com/questions/12703645/how-to-resolve-installutil-is-not-recognized-as-an-internal-or-external-comma
		/// </summary>
		static void Main(string[] args) {

			if (Environment.UserInteractive) {

				string parameter = string.Concat(args);

				switch (parameter) {
					case "--install":
						ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
						break;
					case "--uninstall":
						ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
						break;
				}

			} else {
				ServiceBase[] ServicesToRun;

				ServicesToRun = new ServiceBase[] { 
					new KwhEmailNotificationService() 
				};

				ServiceBase.Run(ServicesToRun);
			}
		}
	}
}
