-- Create new database
CREATE DATABASE ASQL_A1;

GO



-- Create new tables for the application
USE ASQL_A1;

CREATE TABLE Plcs (
	PlcId int,
	CONSTRAINT Pk_PlcId PRIMARY KEY (PlcId)
);

INSERT INTO Plcs (PlcId) VALUES (1), (2), (3);

CREATE TABLE KwhUsage (
	KwhUsageId int IDENTITY(1,1),
	PlcId int,
	ReadTime DateTime NOT NULL,
	Kwh int NOT NULL,
	CONSTRAINT Pk_KwhUsageId PRIMARY KEY (KwhUsageId),
	CONSTRAINT Fk_PlcId FOREIGN KEY (PlcId) REFERENCES Plcs(PlcId)
);

-- Constrain table to single row
CREATE TABLE EmailNotificationConfig (
	Lock char(1) NOT NULL DEFAULT 'X',
	Email varchar(128) NOT NULL,
	MaxKwh int NOT NULL,
	CONSTRAINT Pk_Lock PRIMARY KEY (Lock),
	CONSTRAINT Ck_Lock CHECK (Lock = 'X')
);

GO



-- Create procedures for updating/selecting data from our database
CREATE PROCEDURE AddKwhUsage
	@PlcId int,
	@ReadTime DateTime,
	@Kwh int
AS
	INSERT INTO KwhUsage (PlcId, ReadTime, Kwh)
	VALUES (@PlcId, @ReadTime, @Kwh);

GO



CREATE PROCEDURE FetchKwhUsageByDateTimeRange
	@LowerBound DateTime,
	@UpperBound DateTime
AS
	SELECT KwhUsageId, PlcId, ReadTime, Kwh FROM KwhUsage
	WHERE ReadTime >= @LowerBound AND ReadTime <= @UpperBound
	ORDER BY ReadTime ASC;

GO



-- Apparently this "UPSERT" procedure is not thread safe...
CREATE PROCEDURE UpdateEmailNotificationConfig
	@Email varchar(128),
	@MaxKwh int
AS
	-- No WHERE condition since the table only has one row
	UPDATE EmailNotificationConfig SET Email = @Email, MaxKwh = @MaxKwh;
	
	IF @@ROWCOUNT = 0
	INSERT INTO EmailNotificationConfig (Email, MaxKwh) VALUES (@Email, @MaxKwh);

GO



CREATE PROCEDURE FetchEmailNotificationConfig
AS
	-- No WHERE condition since the table only has one row
	SELECT Email, MaxKwh FROM EmailNotificationConfig;

GO



-- Create new login and map new user to the login
-- This user will be used for reading/writing kWh usage
CREATE LOGIN asql_a1 WITH PASSWORD = 'conestoga1',
	DEFAULT_DATABASE = ASQL_A1;

GO

USE ASQL_A1;

CREATE USER asql_a1_user FROM LOGIN asql_a1;

GO

GRANT EXECUTE ON [dbo].[AddKwhUsage] TO asql_a1_user;
GRANT EXECUTE ON [dbo].[FetchKwhUsageByDateTimeRange] TO asql_a1_user;
GRANT EXECUTE ON [dbo].[UpdateEmailNotificationConfig] TO asql_a1_user;
GRANT EXECUTE ON [dbo].[FetchEmailNotificationConfig] TO asql_a1_user;

GO