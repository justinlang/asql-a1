﻿/*
 * File:		TcpClientWrapperFactory.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains TcpClientWrapperFactory class.
 */


using System.Net.Sockets;


namespace TcpClientServer.TcpConnection {

	/// <summary>
	/// Implementation of ITcpClientFactory which returns a TcpClientWrapper as the ITcpClient implementation.
	/// </summary>
	public class TcpClientWrapperFactory : ITcpClientFactory {

		/// <summary>
		/// Instantiates a .NET TcpClient for TcpClientWrapper and returns the TcpClientWrapper.
		/// </summary>
		/// <returns></returns>
		ITcpClient ITcpClientFactory.Create() {

			TcpClient tcpClient = new TcpClient();
			return new TcpClientWrapper(tcpClient);
		}
	}
}
