﻿/*
 * File:		ITcpClientFactory.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ITcpClientFactory interface.
 */


namespace TcpClientServer.TcpConnection {

	/// <summary>
	/// Contract for an ITcpClient factory.
	/// </summary>
	public interface ITcpClientFactory {

		ITcpClient Create();
	}
}
