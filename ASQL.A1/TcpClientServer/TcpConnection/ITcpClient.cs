﻿/*
 * File:		ITcpClient.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ITcpClient interface.
 */


using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;


namespace TcpClientServer.TcpConnection {

	/// <summary>
	/// Contract for a Tcp client.
	/// </summary>
	public interface ITcpClient : IDisposable {

		void Close();
		NetworkStream GetStream();
		void Connect(IPAddress ip, int port);
		Task ConnectAsync(IPAddress ip, int port);
		bool IsConnected { get; }
		IPAddress RemoteEndpoint { get; }
	}
}
