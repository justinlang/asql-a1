﻿/*
 * File:		TcpClientWrapper.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains TcpClientWrapper class.
 */


using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;


namespace TcpClientServer.TcpConnection {

	/// <summary>
	/// Implements ITcpClient to wrap the .NET TcpClient class.
	/// </summary>
	internal class TcpClientWrapper : ITcpClient {

		private readonly TcpClient _tcpClient;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="tcpClient"></param>
		public TcpClientWrapper(TcpClient tcpClient) {
			_tcpClient = tcpClient;
		}


		/// <summary>
		/// Returns the underlying TcpClient NetworkStream.
		/// </summary>
		/// <returns></returns>
		NetworkStream ITcpClient.GetStream() {
			return _tcpClient.GetStream();
		}


		/// <summary>
		/// Closes the underlying TcpClient.
		/// </summary>
		void ITcpClient.Close() {
			_tcpClient.Close();
		}


		/// <summary>
		/// Disposes the underlying TcpClient.
		/// </summary>
		void IDisposable.Dispose() {
			_tcpClient.Close();
		}


		/// <summary>
		/// Attempts to connect the underlying TcpClient to the given IP and port.
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		void ITcpClient.Connect(IPAddress ip, int port) {
			_tcpClient.Connect(ip, port);
		}


		/// <summary>
		/// Attempts to connect the underlying TcpClient to the given IP and port as an async action.
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		async Task ITcpClient.ConnectAsync(IPAddress ip, int port) {
			await _tcpClient.ConnectAsync(ip, port);
		}


		/// <summary>
		/// Returns true if the underlying TcpClient is connected.
		/// </summary>
		bool ITcpClient.IsConnected {
			get { return _tcpClient.Connected; }
		}


		/// <summary>
		/// Gets the remote IP endpoint for the underlying TcpClient.
		/// </summary>
		IPAddress ITcpClient.RemoteEndpoint {
			get { return ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Address; }
		}
	}
}
