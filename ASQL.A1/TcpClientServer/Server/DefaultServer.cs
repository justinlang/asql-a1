﻿/*
 * File:		DefaultServer.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultServer class.
 */


using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TcpClientServer.TcpConnection;
using TcpClientServer.TcpServer;


namespace TcpClientServer.Server {

	/// <summary>
	/// Implementation of IServer which uses an underlying ITcpServer to listen for requests.
	/// </summary>
	public class DefaultServer : IServer {

		private event ShutdownEventHandler _handleShutdown;
		private readonly ITcpServerFactory _tcpServerFactory;


		/// <summary>
		/// Attempt to prevent reinitialization of the ITcpServer.
		/// </summary>
		private ITcpServer _tcpServer;
		private ITcpServer TcpServer {
			get { return _tcpServer; }
			set {
				if (_tcpServer == null) {
					_tcpServer = value;
				}
			}
		}


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="tcpServerFactory"></param>
		public DefaultServer(
			ITcpServerFactory tcpServerFactory
			) {
			_tcpServerFactory = tcpServerFactory;
		}



		/// <summary>
		/// Starts the server as a separate long running process, which handles each request asynchronously.
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		async Task IServer.LaunchAsync(IPAddress ip, int port, Func<ITcpClient, Task> handleClient) {

			// Start a new task, which itself will return a task on completion.
			// (Note the async lambda expression.)  Thus we need to two awaits.
			await await Task.Factory.StartNew(async () => {

				this.TcpServer = _tcpServerFactory.Create(ip, port);

				try {
					this.TcpServer.Start();

					while (true) {

						using (ITcpClient tcpClient = await this.TcpServer.AcceptTcpConnectionAsync()) {
							// This is handled asynchronously but not concurrently.
							// This whole work flow could possibly be refactored...
							await handleClient(tcpClient);
						}
					}
				} catch (SocketException se) {

				} finally {
					this.OnShutdown();
				}

			}, TaskCreationOptions.LongRunning);
		}


		/// <summary>
		/// Add/remove event handlers for server shutdown.
		/// </summary>
		event ShutdownEventHandler IServer.Shutdown {
			add { _handleShutdown += value; }
			remove { _handleShutdown -= value; }
		}


		/// <summary>
		/// Stop the underlying ITcpServer.
		/// 
		/// This will cause a SocketException which in turn will cause OnShutdown to be called
		/// which in turn will call all shutdown event handlers.
		/// </summary>
		void IServer.Stop() {
			this.TcpServer.Stop();
		}


		/// <summary>
		/// Call event handlers for server shutdown.
		/// </summary>
		private void OnShutdown() {
			if (_handleShutdown != null) {
				_handleShutdown(this, new EventArgs());
			}
		}
	}
}
