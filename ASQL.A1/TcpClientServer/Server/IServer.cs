﻿/*
 * File:		IServer.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ShutdownEventHandler delegate and IServer interface.
 */


using System;
using System.Net;
using System.Threading.Tasks;
using TcpClientServer.TcpConnection;


namespace TcpClientServer.Server {

	/// <summary>
	/// Delegate for handling server shutdown.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="args"></param>
	public delegate void ShutdownEventHandler(object sender, EventArgs args);


	/// <summary>
	/// Contract for a server which handles incoming requests from ITcpClients.
	/// </summary>
	public interface IServer {

		event ShutdownEventHandler Shutdown;
		Task LaunchAsync(IPAddress ip, int port, Func<ITcpClient, Task> handleClient);
		void Stop();
	}
}
