﻿/*
 * File:		TcpListenerWrapper.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains TcpListenerWrapper class.
 */


using System.Net.Sockets;
using System.Threading.Tasks;
using TcpClientServer.TcpConnection;


namespace TcpClientServer.TcpServer {

	/// <summary>
	/// Implements ITcpServer to wrap the .NET TcpListener class.
	/// </summary>
	internal class TcpListenerWrapper : ITcpServer {

		private readonly TcpListener _tcpListener;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="tcpListener"></param>
		public TcpListenerWrapper(TcpListener tcpListener) {
			_tcpListener = tcpListener;
		}


		/// <summary>
		/// Starts the underlying TcpListener.
		/// </summary>
		void ITcpServer.Start() {
			_tcpListener.Start();
		}


		/// <summary>
		/// Stops the underlying TcpListener.
		/// </summary>
		void ITcpServer.Stop() {
			_tcpListener.Stop();
		}


		/// <summary>
		/// The underlying TcpListener waits for a connection and returns a .NET TcpClient wrapped
		/// as a TcpClientWrapper.
		/// </summary>
		/// <returns></returns>
		ITcpClient ITcpServer.AcceptTcpConnection() {
			return new TcpClientWrapper(_tcpListener.AcceptTcpClient());
		}


		/// <summary>
		/// The underlying TcpListener listens for a connection and returns a .NET TcpClient
		/// wrapped as a TcpClientWrapper, as an async action.
		/// </summary>
		/// <returns></returns>
		async Task<ITcpClient> ITcpServer.AcceptTcpConnectionAsync() {
			return new TcpClientWrapper(await _tcpListener.AcceptTcpClientAsync());
		}
	}
}
