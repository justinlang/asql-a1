﻿/*
 * File:		TcpListenerWrapperFactory.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains TcpListenerWrapperFactory class.
 */


using System.Net;
using System.Net.Sockets;


namespace TcpClientServer.TcpServer {

	/// <summary>
	/// Implementation of ITcpServerFactory which returns a TcpListenerWrapper as the ITcpServer implementation.
	/// </summary>
	public class TcpListenerWrapperFactory : ITcpServerFactory {

		/// <summary>
		/// Constructor.
		/// </summary>
		public TcpListenerWrapperFactory() { }


		/// <summary>
		/// Instantiates a .NET TcpListener for TcpListenerWrapper and returns the TcpListenerWrapper.
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		ITcpServer ITcpServerFactory.Create(IPAddress ip, int port) {

			TcpListener tcpListener = new TcpListener(ip, port);
			return new TcpListenerWrapper(tcpListener);
		}
	}
}
