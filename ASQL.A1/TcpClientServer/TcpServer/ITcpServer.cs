﻿/*
 * File:		ITcpServer.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ITcpServer interface.
 */


using System.Threading.Tasks;
using TcpClientServer.TcpConnection;


namespace TcpClientServer.TcpServer {

	/// <summary>
	/// Contract for a Tcp server.
	/// </summary>
	public interface ITcpServer {

		void Start();
		void Stop();
		ITcpClient AcceptTcpConnection();
		Task<ITcpClient> AcceptTcpConnectionAsync();
	}
}
