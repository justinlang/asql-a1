﻿/*
 * File:		ITcpServerFactory.cs
 * Project:		TcpClientServer
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains ITcpServerFactory interface.
 */


using System.Net;


namespace TcpClientServer.TcpServer {

	/// <summary>
	/// Contract for an ITcpServer factory.
	/// </summary>
	public interface ITcpServerFactory {

		ITcpServer Create(IPAddress ip, int port);
	}
}
