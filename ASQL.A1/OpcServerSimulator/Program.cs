﻿/*
 * File:		Program.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains Program class.
 */


using OpcServerSimulator.Application;
using StructureMap;


namespace OpcServerSimulator {

	/// <summary>
	/// Harness for starting application and intializing IoC.
	/// </summary>
	class Program {

		/// <summary>
		/// Program entry point.
		/// </summary>
		/// <param name="args"></param>
		static void Main( string[] args ) {

			IoC.Initialize();
			IApplication application = ObjectFactory.GetInstance<IApplication>();
			application.Run(args);
		}
	}
}
