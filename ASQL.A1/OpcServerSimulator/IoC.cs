﻿/*
 * File:		IoC.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IoC static class.
 */



using OpcServerSimulator.Application;
using StructureMap;
using TcpClientServer.TcpConnection;
using OpcServerSimulator.Repository;


namespace OpcServerSimulator {

	/// <summary>
	/// Inversion of control container for application.
	/// </summary>
	internal static class IoC {

		/// <summary>
		/// Wraps initializing StructureMap container.
		/// </summary>
		public static void Initialize() {

			ObjectFactory.Configure(
				config => {
					config.Scan(
						scan => {
							scan.TheCallingAssembly();
						});

					// Singletons
					config.For<IApplication>().Singleton().Use<DefaultApplication>();
					config.For<IKwhUsageRepository>().Singleton().Use<DefaultKwhUsageRespository>();

					// Fresh instances
					config.For<ITcpClientFactory>().Use<TcpClientWrapperFactory>();
				});
		}
	}
}
