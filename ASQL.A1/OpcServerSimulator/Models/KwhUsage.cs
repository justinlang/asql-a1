﻿/*
 * File:		KwhUsage.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains KwhUsage class.
 */


using System;


namespace OpcServerSimulator.Models {

	/// <summary>
	/// Represents kWh usage for a given Plc.
	/// </summary>
	internal class KwhUsage {

		private readonly int _plcId;
		private readonly DateTime _readTime;
		private readonly int _kwh;

		public KwhUsage(
			int plcId,
			DateTime readTime,
			int kwhUsed
			) {

			_plcId = plcId;
			_readTime = readTime;
			_kwh = kwhUsed;
		}

		public int PlcId {
			get { return _plcId; }
		}

		public DateTime ReadTime {
			get { return _readTime; }
		}

		public int Kwh {
			get { return _kwh; }
		}
	}
}
