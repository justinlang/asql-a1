﻿/*
 * File:		DefaultApplication.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultApplication class.
 */


using System;
using System.Text;
using TcpClientServer.TcpConnection;
using System.Net;
using System.Net.Sockets;
using System.IO;
using OpcServerSimulator.Repository;
using OpcServerSimulator.Models;


namespace OpcServerSimulator.Application {

	/// <summary>
	/// Main logic and control flow for application.
	/// 
	/// Uses a Tcp client to request Plc kWh usage from the PlcSimulatorService.
	/// </summary>
	internal class DefaultApplication : IApplication {

		private const int DEFAULT_PORT = 1000;
		private const char DELIMITER = '|';
		private const char READ_COMMAND = 'R';
		private const char RESULT_STATUS = 'R';
		private const char FAULT_STATUS = 'F';

		private readonly ITcpClientFactory _tcpClientFactory;
		private readonly IKwhUsageRepository _repository;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="tcpClientFactory"></param>
		/// <param name="repository"></param>
		public DefaultApplication(
			ITcpClientFactory tcpClientFactory,
			IKwhUsageRepository repository
			) {
			_tcpClientFactory = tcpClientFactory;
			_repository = repository;
		}


		/// <summary>
		/// Check arguments and start main application loop - wait for
		/// user input and request Plc kWh.
		/// </summary>
		/// <param name="args"></param>
		void IApplication.Run(string[] args) {

			int port = DEFAULT_PORT;
			IPAddress ip;

			if (args.Length == 0 || args.Length > 2) {
				Console.WriteLine(Lang.UsageStatement);
				return;
			}
			
			if (!IPAddress.TryParse(args[0], out ip)) {
				Console.WriteLine(Lang.InvalidIp);
				return;
			}

			if (args.Length == 2 && !Int32.TryParse(args[1], out port)) {
				Console.WriteLine(Lang.InvalidPort);
				return;
			}

			Console.WriteLine(Lang.ExitInstructions);
			Console.WriteLine(Lang.EnterPlcNumber);
			Console.WriteLine();

			bool errorOccurred = false;
			ConsoleKeyInfo keyInfo;
			while (!errorOccurred && (keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Q) {

				char[] validPlcOptions = new char[] { '1', '2', '3' };
				if (Array.IndexOf(validPlcOptions, keyInfo.KeyChar) != -1) {

					try {
						SendTcpRequest(port, ip, keyInfo.KeyChar);
					} catch (SocketException se) {
						Console.WriteLine(Lang.UnableToConnectToPlcService);
						errorOccurred = true;
					}
				}
			}
		}


		/// <summary>
		/// Send a Tcp request according to assignment protocol.
		/// </summary>
		/// <param name="port"></param>
		/// <param name="ip"></param>
		/// <param name="plcId"></param>
		private void SendTcpRequest(int port, IPAddress ip, char plcId) {

			using (ITcpClient tcpClient = _tcpClientFactory.Create()) {

				tcpClient.Connect(ip, port);

				using (NetworkStream networkStream = tcpClient.GetStream())
				using (StreamReader streamReader = new StreamReader(networkStream))
				using (StreamWriter streamWriter = new StreamWriter(networkStream)) {

					Console.WriteLine(string.Format(Lang.SendingRequestForPlc, plcId));

					string request = new StringBuilder()
						.Append(READ_COMMAND)
						.Append(DELIMITER)
						.Append(plcId)
						.Append(Environment.NewLine)
						.ToString();

					streamWriter.WriteLine(request);
					streamWriter.Flush();

					Console.WriteLine(Lang.WaitingForResponse);
					string response = streamReader.ReadLine();
					Console.WriteLine(string.Format(Lang.ServerRespondedWith, response));

					HandleResponse(response);
				}
			}
		}


		/// <summary>
		/// Handle response from PlcSimulatorService.
		/// </summary>
		/// <param name="response"></param>
		private void HandleResponse(string response) {

			if (!string.IsNullOrEmpty(response)) {

				switch (response[0]) {

					case RESULT_STATUS:

						KwhUsage kwhUsage;
						if (this.TryHandleResultStatus(response, out kwhUsage)) {

							Console.WriteLine(string.Format(Lang.KwhConsumption, kwhUsage.PlcId, kwhUsage.Kwh));
							Console.WriteLine();

							AddToRepository(kwhUsage);
						}
						break;

					case FAULT_STATUS:
					default:
						Console.WriteLine();
						break;
				}
			}
		}


		/// <summary>
		/// Use IRepository object to persist data.
		/// </summary>
		/// <param name="kwhUsage"></param>
		private void AddToRepository(KwhUsage kwhUsage) {

			try {
				Console.WriteLine(Lang.WritingToDatabase);
				_repository.Add(kwhUsage);
				Console.WriteLine(Lang.DataRecordedSuccessfully);

			} catch (RepositoryException re) {
				Console.WriteLine(Lang.DatabaseErrorOccurred);
				Console.WriteLine(string.Format(Lang.ErrorInfo, re.InnerException.Message));
			}

			Console.WriteLine();
		}


		/// <summary>
		/// Parse kWh data from response string.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="kwhUsage"></param>
		/// <returns></returns>
		private bool TryHandleResultStatus(string response, out KwhUsage kwhUsage) {

			if (!string.IsNullOrEmpty(response) && response.Length > 1) {

				int delimiterIndex = response.IndexOf(DELIMITER);
				int plcIdIndex = delimiterIndex + 1;
				int kwhIndex = plcIdIndex + 1;
				int lastIndex = response.Length - 1;
				string plcIdString = string.Empty;
				string kwhString = string.Empty;

				if (kwhIndex <= lastIndex) {
					plcIdString = response.Substring(plcIdIndex, 1);
					kwhString = response.Substring(kwhIndex, lastIndex - plcIdIndex);

					int plcId, kwh;
					if (Int32.TryParse(plcIdString, out plcId) && Int32.TryParse(kwhString, out kwh)) {

						kwhUsage = new KwhUsage(plcId, DateTime.Now, kwh);
						return true;
					}
				}
			}

			kwhUsage = null;
			return false;
		}
	}
}
