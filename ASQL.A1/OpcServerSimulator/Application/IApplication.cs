﻿/*
 * File:		IApplication.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IApplication interface.
 */


namespace OpcServerSimulator.Application {

	/// <summary>
	/// Interface for running an application with arguments.
	/// </summary>
	internal interface IApplication {

		void Run(string[] args);
	}
}
