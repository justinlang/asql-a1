﻿/*
 * File:		DefaultKwhUsageRespository.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultKwhUsageRespository class.
 */


using System;
using System.Configuration;
using System.Data.SqlClient;


namespace OpcServerSimulator.Repository {

	/// <summary>
	/// Uses a SqlConnection to persist kWh usage for a Plc.
	/// </summary>
	class DefaultKwhUsageRespository : IKwhUsageRepository {

		public DefaultKwhUsageRespository() { }

		/// <summary>
		/// Calls the AddKwhUsage sproc in a Sql database to persist KwhUsage model,
		/// using a connection string from ConfigurationManager.
		/// </summary>
		/// <param name="kwhUsage"></param>
		void IKwhUsageRepository.Add(Models.KwhUsage kwhUsage) {
			
			string connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			using (SqlCommand command = new SqlCommand("AddKwhUsage", connection)) {

				try {
					connection.Open();

					command.CommandType = System.Data.CommandType.StoredProcedure;

					command.Parameters.Add(new SqlParameter("@PlcId", System.Data.SqlDbType.Int));
					command.Parameters["@PlcId"].Value = kwhUsage.PlcId;

					command.Parameters.Add(new SqlParameter("@ReadTime", System.Data.SqlDbType.DateTime));
					command.Parameters["@ReadTime"].Value = kwhUsage.ReadTime;

					command.Parameters.Add(new SqlParameter("@Kwh", System.Data.SqlDbType.Int));
					command.Parameters["@Kwh"].Value = kwhUsage.Kwh;

					int rowsAffected = command.ExecuteNonQuery();

				} catch (SqlException se) {
					throw new RepositoryException("A database error occurred.", se);
				}
			}
		}
	}
}
