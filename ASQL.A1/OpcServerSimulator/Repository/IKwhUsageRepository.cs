﻿/*
 * File:		IKwhUsageRepository.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IKwhUsageRepository interface.
 */

using OpcServerSimulator.Models;


namespace OpcServerSimulator.Repository {

	/// <summary>
	/// Constract for persisting kWh usage.
	/// </summary>
	internal interface IKwhUsageRepository {

		void Add(KwhUsage kwhUsage);
	}
}
