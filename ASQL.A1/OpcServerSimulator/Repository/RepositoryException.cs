﻿/*
 * File:		RepositoryException.cs
 * Project:		OpcServerSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains RepositoryException class.
 */


using System;


namespace OpcServerSimulator.Repository {

	/// <summary>
	/// Custom exception to be thrown by repository operations, to hide exceptions
	/// from implementations of IRepository.
	/// </summary>
	class RepositoryException : Exception {

		public RepositoryException() {}

		public RepositoryException(string message)
			: base(message) { }

		public RepositoryException(string message, Exception innerException)
			: base(message, innerException) { }
	}
}
