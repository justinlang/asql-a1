﻿/*
 * File:		IoC.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IoC static class.
 */


using PlcServiceSimulator.Application;
using TcpClientServer.Server;
using TcpClientServer.TcpServer;
using StructureMap;
using PlcServiceSimulator.Plcs;


namespace PlcServiceSimulator {

	/// <summary>
	/// Inversion of control container for application.
	/// </summary>
	internal static class IoC {

		/// <summary>
		/// Wraps initializing StructureMap container.
		/// </summary>
		public static void Initialize() {

			ObjectFactory.Configure(
				config => {
					config.Scan(
						scan => {
							scan.TheCallingAssembly();
						});

					// Singletons
					config.For<IApplication>().Singleton().Use<DefaultApplication>();

					// Fresh instances
					config.For<IServer>().Use<DefaultServer>();
					config.For<ITcpServerFactory>().Use<TcpListenerWrapperFactory>();
					config.For<IPlcSimulator>().Use<DefaultPlcSimulator>();
				});
		}
	}
}