﻿/*
 * File:		DefaultApplication.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultApplication class.
 */



using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using PlcServiceSimulator.Plcs;
using TcpClientServer.Server;
using TcpClientServer.TcpConnection;

namespace PlcServiceSimulator.Application {

	/// <summary>
	/// Main logic and control flow for program.
	/// 
	/// Starts up a PlcServiceSimulator, with two main components: a Tcp server
	/// listening for and responding to connections, and a Plc simulator, which runs on 
	/// a separate thread and generates pulses.
	/// </summary>
	internal class DefaultApplication : IApplication {

		private const int DEFAULT_PORT = 1000;
		private const char DELIMITER = '|';
		private const char READ_COMMAND = 'R';
		private const char RESULT_STATUS = 'R';
		private const char FAULT_STATUS = 'F';

		private readonly IServer _server;
		private readonly IPlcSimulator _plcSimulator;
		private readonly Random _rnd;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="server"></param>
		/// <param name="plcSimulator"></param>
		public DefaultApplication(
			IServer server,
			IPlcSimulator plcSimulator
			) {
			_server = server;
			_plcSimulator = plcSimulator;

			_rnd = new Random();
		}


		/// <summary>
		/// Parses arguments and launches the server and simulator.
		/// </summary>
		/// <param name="args"></param>
		void IApplication.Run(string[] args) {

			int port = DEFAULT_PORT;
			IPAddress ip;

			if (args.Length == 0 || args.Length > 2) {
				Console.WriteLine(Lang.UsageStatement);
				return;
			}

			if (!IPAddress.TryParse(args[0], out ip)) {
				Console.WriteLine(Lang.InvalidIp);
				return;
			}

			if (args.Length == 2 && !Int32.TryParse(args[1], out port)) {
				Console.WriteLine(Lang.InvalidPort);
				return;
			}

			StartPlcSimulator();
			StartServer(port, ip);
		}


		/// <summary>
		/// Starts the Plc simulator.
		/// </summary>
		private void StartPlcSimulator() {
			_plcSimulator.Start();
		}


		/// <summary>
		/// Starts the Tcp server and sets up handlers for shutdown and connections.
		/// Contains the main application loop, which instructs the Plc simulator to fire pulses.
		/// </summary>
		/// <param name="port"></param>
		/// <param name="ip"></param>
		private void StartServer(int port, IPAddress ip) {

			_server.Shutdown += (sender, eventArgs) => {
				Console.WriteLine();
				Console.WriteLine(Lang.ServerShuttingDown);
			};

			Console.WriteLine(Lang.ExitInstructions);
			Console.WriteLine(Lang.ServerRunning);
			Console.WriteLine(string.Format(Lang.ListeningOnIpPort, ip, port));
			Console.WriteLine();

			// Not using await here will cause this method to execute in parallel with the following code
			Task serverTask = _server.LaunchAsync(ip, port, this.HandleTcpConnection);

			int pulseLimiter = 0;
			while (!(serverTask.IsCanceled || serverTask.IsCompleted || serverTask.IsFaulted)) {

				pulseLimiter++;
				if (pulseLimiter % 10000 == 0) {
					_plcSimulator.SendPulse();
				}

				if (Console.KeyAvailable) {
					if (Console.ReadKey(true).Key == ConsoleKey.Q) {
						_server.Stop();
					}
				}
			}
		}


		/// <summary>
		/// Handles Tcp connections to the server.
		/// Parse message according to protocol and respond with the requested Plc kWh.
		/// </summary>
		/// <param name="tcpClient"></param>
		/// <returns></returns>
		private async Task HandleTcpConnection(ITcpClient tcpClient) {

			Console.WriteLine(string.Format(Lang.ProcessingConnectionFromClient, tcpClient.RemoteEndpoint));

			using (NetworkStream networkStream = tcpClient.GetStream())
			using (StreamReader streamReader = new StreamReader(networkStream))
			using (StreamWriter streamWriter = new StreamWriter(networkStream)) {

				string request = await streamReader.ReadLineAsync();
				Console.WriteLine(string.Format(Lang.RequestReceived, request));

				char responseStatus = FAULT_STATUS;
				string response = string.Empty;

				char requestCommand;
				if (!string.IsNullOrEmpty(request) && this.TryParseCommand(request, out requestCommand)) {

					switch (requestCommand) {
						case READ_COMMAND:

							string plcId = string.Empty;
							if (TryParsePlcId(request, out plcId)) {

								int kwh = this.FetchKwhForPlc(plcId);
								this.ResetKwhForPlc(plcId);

								responseStatus = RESULT_STATUS;

								response = new StringBuilder()
									.Append(RESULT_STATUS)
									.Append(DELIMITER)
									.Append(plcId)
									.Append(kwh)
									.Append(Environment.NewLine)
									.ToString();
							}

							break;

						default:
							break;
					}
				}

				// Send a Fault response if the request was unreadable
				// OR send a Fault response 1 times out of 100
				if (responseStatus == FAULT_STATUS || _rnd.Next(0, 100) == 99) {

					response = new StringBuilder()
						.Append(FAULT_STATUS)
						.Append(Environment.NewLine)
						.ToString();
				}

				await this.SendResponse(streamWriter, response);
			}
		}


		/// <summary>
		/// Writes a string to an open StreamWriter.
		/// </summary>
		/// <param name="streamWriter"></param>
		/// <param name="response"></param>
		/// <returns></returns>
		private async Task SendResponse(StreamWriter streamWriter, string response) {

			Console.WriteLine(string.Format(Lang.SendingResponse, response));
			await streamWriter.WriteLineAsync(response);
			streamWriter.Flush();
		}


		/// <summary>
		/// Attempts to parse the Plc id out of a request string.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="plcId"></param>
		/// <returns></returns>
		private bool TryParsePlcId(string request, out string plcId) {

			int delimiterIndex = request.IndexOf(DELIMITER);
			int lastIndex = request.Length - 1;

			// Make sure we're not substringing outside of array bounds
			if (delimiterIndex < lastIndex) {
				plcId = request.Substring(delimiterIndex + 1, lastIndex - delimiterIndex);

				string[] validPlcIds = new string[] { "1", "2", "3" };
				if (Array.IndexOf(validPlcIds, plcId) != -1) {
					return true;
				}
			}

			plcId = string.Empty;
			return false;
		}


		/// <summary>
		/// Instructs the Plc simulator to reset the totals for a given Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		private void ResetKwhForPlc(string plcId) {
			// Unsafely parsing the plcId
			_plcSimulator.ResetKwhForPlc(Int32.Parse(plcId));
		}


		/// <summary>
		/// Fetches the kWh for a given Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		/// <returns></returns>
		private int FetchKwhForPlc(string plcId) {
			// Unsafely parsing the plcId
			return _plcSimulator.GetKwhForPlc(Int32.Parse(plcId));
		}


		/// <summary>
		/// Attempts to parse the command from a request string.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="command"></param>
		/// <returns></returns>
		private bool TryParseCommand(string request, out char command) {

			if (!string.IsNullOrEmpty(request) && request.Length > 0) {
				command = request[0];
				return true;
			}

			command = '\0';
			return false;
		}
	}
}
