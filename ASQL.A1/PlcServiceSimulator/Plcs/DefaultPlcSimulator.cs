﻿/*
 * File:		DefaultPlcSimulator.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains DefaultPlcSimulator class.
 */


using System.Collections.Generic;
using PlcServiceSimulator.Plcs;
using Retlang.Channels;


namespace PlcServiceSimulator.Plcs {

	/// <summary>
	/// Simulates some number of Plcs running concurrently, generating pulses.
	/// </summary>
	internal class DefaultPlcSimulator : IPlcSimulator {

		private readonly IChannel<bool> _commandChannel;
		private readonly List<Plc> _plcs;
		private readonly PulseReceiver _pulseReceiver;


		/// <summary>
		/// Constructor.
		/// 
		/// Initialize Retlang channels for communication.  Plcs are instructed to 
		/// generate Pulses concurrently which are received by the PulseReceiver.
		/// </summary>
		public DefaultPlcSimulator() {

			_commandChannel = new Channel<bool>();
			IChannel<Pulse> pulseChannel = new Channel<Pulse>();

			_pulseReceiver = new PulseReceiver(pulseChannel, 3);
			_plcs = new List<Plc>() {
				new Plc(_commandChannel, pulseChannel, 1),
				new Plc(_commandChannel, pulseChannel, 2),
				new Plc(_commandChannel, pulseChannel, 3)
			};
		}


		/// <summary>
		/// Start the receiver and Plcs executing concurrently.
		/// </summary>
		void IPlcSimulator.Start() {
			_pulseReceiver.Start();
			_plcs.ForEach(plc => plc.Start());
		}


		/// <summary>
		/// Send a message to Plcs to generate a Pulse.
		/// </summary>
		void IPlcSimulator.SendPulse() {
			bool sendPulse = true;
			_commandChannel.Publish(sendPulse);
		}


		/// <summary>
		/// Request total kWh for a given Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		/// <returns></returns>
		int IPlcSimulator.GetKwhForPlc(int plcId) {
			return _pulseReceiver.GetTotalForPlc(plcId);
		}


		/// <summary>
		/// Reset kWh for a give Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		void IPlcSimulator.ResetKwhForPlc(int plcId) {
			_pulseReceiver.ResetTotalForPlc(plcId);
		}
	}
}
