﻿/*
 * File:		Pulse.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains Pulse class.
 */


namespace PlcServiceSimulator.Plcs {

	/// <summary>
	/// Represents a pulse from a Plc.
	/// </summary>
	internal class Pulse {

		public int PlcId { get; set; }
	}
}
