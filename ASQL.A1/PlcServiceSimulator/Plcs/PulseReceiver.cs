﻿/*
 * File:		PulseReceiver.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains PulseReceiver class.
 */


using Retlang.Channels;
using Retlang.Fibers;


namespace PlcServiceSimulator.Plcs {

	/// <summary>
	/// Receives and tracks pulses from each concurrent Plc.
	/// </summary>
	internal class PulseReceiver {

		private readonly IFiber _fiber;
		private readonly IChannel<Pulse> _pulseChannel;
		private int[] _pulseTotals;
		private object _pulseTotalsLock;


		/// <summary>
		/// Constructor.
		/// 
		/// Initialize Retlang channels for communication.
		/// Initialize the array for holding kWh totals.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="numberOfPlcs"></param>
		public PulseReceiver(IChannel<Pulse> channel, int numberOfPlcs) {

			_pulseTotals = new int[numberOfPlcs];
			_pulseChannel = channel;
			_fiber = new PoolFiber();
			_pulseTotalsLock = new object();
		}


		/// <summary>
		/// Start the fiber and subscribes to the pulseChannel.
		/// </summary>
		public void Start() {
			_fiber.Start();
			_pulseChannel.Subscribe(_fiber, this.UpdatePulseTotals);
		}


		/// <summary>
		/// Handle messages on the pulseChannel.  Update the correct Plc total.
		/// </summary>
		/// <param name="pulse"></param>
		private void UpdatePulseTotals(Pulse pulse) {

			// This occurs in the execution context of the IFiber.
			// If we were just dealing with the published Pulses from the channel, I think
			// we wouldn't need the lock, since I THINK the published messages would be taken
			// from a queue (i.e. each Plc is publishing concurrently, but Retlang enqueues
			// the messages).

			lock (_pulseTotalsLock) {
				// Unsafe array access
				_pulseTotals[pulse.PlcId - 1]++;
			}
		}


		/// <summary>
		/// Fetch the total kWh for a given Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		/// <returns></returns>
		public int GetTotalForPlc(int plcId) {

			// However, this method and the next access the array in the application thread, not in the IFiber,
			// and I think there could potentially be a race condition here.  I.e. the array
			// is being updated from the IFiber, but I think I'm reading from it and resetting values
			// concurrently in a different thread.

			lock (_pulseTotalsLock) {

				if (plcId <= _pulseTotals.Length) {
					// Unsafe array access
					int currentTotal = _pulseTotals[plcId - 1];
					return currentTotal;
				}
			}

			return -1;
		}


		/// <summary>
		/// Reset the total for a given Plc id.
		/// </summary>
		/// <param name="plcId"></param>
		public void ResetTotalForPlc(int plcId) {

			lock (_pulseTotalsLock) {

				if (plcId <= _pulseTotals.Length) {
					// Unsafe array access
					_pulseTotals[plcId - 1] = 0;
				}
			}
		}
	}
}
