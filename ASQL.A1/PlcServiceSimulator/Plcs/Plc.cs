﻿/*
 * File:		Plc.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains Plc class.
 */


using System.Threading;
using Retlang.Channels;
using Retlang.Fibers;


namespace PlcServiceSimulator.Plcs {

	/// <summary>
	/// Represents a single Plc object.
	/// </summary>
	internal class Plc {

		private readonly IChannel<Pulse> _pulsechannel;
		private readonly IChannel<bool> _commandChannel;
		private readonly IFiber _fiber;
		private readonly int _plcId;


		/// <summary>
		/// Constructor.
		/// 
		/// Initialize Retlang channels for communication.
		/// </summary>
		/// <param name="commandChannel"></param>
		/// <param name="pulseChannel"></param>
		/// <param name="plcId"></param>
		public Plc(
			IChannel<bool> commandChannel,
			IChannel<Pulse> pulseChannel,
			int plcId
			) {

			_commandChannel = commandChannel;
			_pulsechannel = pulseChannel;
			_plcId = plcId;

			_fiber = new PoolFiber();
		}


		/// <summary>
		/// Starts the fiber and subscribes to the commandChannel.
		/// </summary>
		public void Start() {
			_fiber.Start();
			_commandChannel.Subscribe(_fiber, SendPulse);
		}


		/// <summary>
		/// Handler for messages on the commandChannel.  If the message is boolean true,
		/// send a pulse on the pulseChannel.
		/// </summary>
		/// <param name="sendPulse"></param>
		private void SendPulse(bool sendPulse) {

			if (sendPulse) {
				// According to Retlang source code, this method is thread safe, so I think we're
				// okay to share instance between fibers
				_pulsechannel.Publish(new Pulse() { PlcId = _plcId });
			}
		}
	}
}
