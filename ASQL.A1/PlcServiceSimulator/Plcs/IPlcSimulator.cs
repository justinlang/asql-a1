﻿/*
 * File:		IPlcSimulator.cs
 * Project:		PlcServiceSimulator
 * Solution:	ASQL.A1
 * Author:		Justin Lang
 * Date:		Sept 16 2013
 * Description:	Contains IPlcSimulator interface.
 */


namespace PlcServiceSimulator.Plcs {

	/// <summary>
	/// Represents the contract for a Plc simulator.
	/// </summary>
	internal interface IPlcSimulator {

		void Start();
		void SendPulse();
		int GetKwhForPlc(int plcId);
		void ResetKwhForPlc(int plcId);
	}
}
